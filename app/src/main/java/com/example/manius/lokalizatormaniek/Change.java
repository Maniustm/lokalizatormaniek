package com.example.manius.lokalizatormaniek;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Change extends Activity {
    private EditText etAddress;
    private EditText etLatitude;
    private EditText etLongitude;
    private Button btnAddressToGeo;
    private Button btnGeoToAddress;
    private Button btnFromYourPos;
    private TextView tvInformations;


    private OnClickListener geoToAddressListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            //Get location from EditText
            double latitude = new Double(etLatitude.getText().toString());
            double longitude = new Double(etLongitude.getText().toString());
            //Geolocation -> addresses
            try {
                List<Address> addresses =
                        geocoder.getFromLocation(latitude, longitude, 1);
                String result = "Lokalizacja adresu:\n";

                for (Address address : addresses) {
                    for(int i = 0, j = address.getMaxAddressLineIndex(); i <= j; i++)
                        result += address.getAddressLine(i) + "\n";
                    result += "\n\n";
                }

                tvInformations.setText(result);
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(),
                        e.toString(),
                        Toast.LENGTH_LONG).show();
            }
        }
    };
    private OnClickListener addressToGeoListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            String location = etAddress.getText().toString();
            try {
                List<Address> addresses = geocoder.getFromLocationName(location, 5);
                String result = "Współrzędne:\n";

                for (Address address : addresses) {
                    for(int i = 0, j = address.getMaxAddressLineIndex(); i <= j; i++)
                        result += address.getAddressLine(i) + "\n";

                    result += "Szerokość: " + address.getLatitude() + "\n" +
                            "Długość: " + address.getLongitude();
                    result += "\n\n";
                }

                tvInformations.setText(result);
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(),
                        e.toString(),
                        Toast.LENGTH_LONG).show();
            }
        }
    };
    private Geocoder geocoder;
    private LocationManager locationManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change);

        etAddress         = (EditText)findViewById(R.id.etAddress);
        etLatitude         = (EditText)findViewById(R.id.etLatitude);
        etLongitude     = (EditText)findViewById(R.id.etLongitude);
        btnAddressToGeo = (Button)findViewById(R.id.btnAddressToGeo);
        btnGeoToAddress = (Button)findViewById(R.id.btnGeoToAddress);
        tvInformations     = (TextView)findViewById(R.id.tvInformations);
        btnAddressToGeo.setOnClickListener(addressToGeoListener);
        btnGeoToAddress.setOnClickListener(geoToAddressListener);


        etLatitude.setText("51.5311945");
        etLongitude.setText("20.0086471");
        etAddress.setText("Tomaszów Mazowiecki");

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        geocoder = new Geocoder(this, Locale.getDefault());
    }
}