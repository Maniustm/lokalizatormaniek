package com.example.manius.lokalizatormaniek;

import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import org.osmdroid.bonuspack.overlays.Marker;
import java.io.IOException;
import java.util.List;

public class Geo extends Activity {

    private MapView osm;
    private MapController mc;
    private String coordinates;
    private String location;
    private LocationManager locationManager;
    private String provider;
    GeoPoint center;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo);

        osm = (MapView) findViewById(R.id.mapview);
        osm.setTileSource(TileSourceFactory.MAPNIK);
        osm.setBuiltInZoomControls(true);
        osm.setMultiTouchControls(true);

        mc = (MapController) osm.getController();
        mc.setZoom(12);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        center = new GeoPoint(new Double(45), new Double(56));
        mc.animateTo(center);
        addMarker(center);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        LocationListener listner = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                center = new GeoPoint(location.getLatitude(), location.getLongitude());
                mc.animateTo(center);
                addMarker(center);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, listner);
    }

    public void addMarker(GeoPoint center) {
        Marker marker = new Marker(osm);
        marker.setPosition(center);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
        marker.setTitle(getAddressMyLocation(center.getLatitude(),center.getLongitude()) );

        osm.getOverlays().clear();
        osm.getOverlays().add(marker);
        osm.invalidate();
    }

    private String getAddressMyLocation(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(getApplicationContext());
        String result = "";
        int z = 0;
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude,
                    longitude, 1);
            for (Address address : addresses) {
                for (int i = 0, j = address.getMaxAddressLineIndex(); i <= j; i++) {

                    // z = z + 1;
                    if (i == 0) {
                        result += address.getAddressLine(i) + " ";
                    }

                    if (i == 1) {
                        String temp[] = address.getAddressLine(i).split(" ");
                        result += temp[temp.length - 1];
                    }

                    // result += address.getAddressLine(i) + "?";

                }

            }
            Log.d("result", z + "");
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return result;
    }



}


