package com.example.manius.lokalizatormaniek;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainActivity extends Activity {

    private Button buttonSimple;
    private Button buttonAdvanced;
    private Button buttonAbout;
    private Button buttonExit;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        context = getApplicationContext();
        buttonSimple = (Button) findViewById(R.id.simpleButton);
        buttonAdvanced = (Button) findViewById(R.id.advancedButton);
        buttonAbout = (Button) findViewById(R.id.aboutButton);
        buttonExit = (Button) findViewById(R.id.ExitButton);

        buttonSimple.setOnClickListener(new SimpleOnClickListener());
        buttonAdvanced.setOnClickListener(new AdvancedOnClickListener());
        buttonAbout.setOnClickListener(new AboutOnClickListener());
        buttonExit.setOnClickListener(new ExitOnClickListener());




    }

    public class SimpleOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            //Toast.makeText(context, "Geo", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, Geo.class);
            startActivity(intent);
        }

    }

    public class AdvancedOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            //Toast.makeText(context, "Change", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, Change.class);
            startActivity(intent);
        }

    }

  public class AboutOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            //Toast.makeText(context, "O mnie", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, AboutActivity.class);
            startActivity(intent);
        }

    }

    public class ExitOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            //Toast.makeText(context, "Zakończ", Toast.LENGTH_LONG).show();
            finish();
        }

    }
}